@extends('layout/main')
@section('menu-member', 'active')
@section('title', 'Member')
@section('content')

    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Edit Form</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal" action="{{url('/update')}}" method="post"> 
            @csrf
            <div class="card-body">
                <div class="col-sm-5">
                    <h4>Nama</h4>
                    <input type="hidden"  class="form-control" name="idMember" value="{{$detail['id']}}" >
                    <input type="text" class="form-control" name="namaMember" value="{{$detail['nama']}}" >
                </div>
                <div class="col-sm-5">
                    <h4>Universitas</h4>
                    <input type="text" class="form-control" name="asalMember" value="{{$detail['asal']}}" >
                </div>
                <div class="col-sm-5">
                    <h4>Asal</h4>
                    <input type="text" class="form-control" name="univ" value="{{$detail['univ']}}" >
                </div>
            </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Save</button>
                <button type="submit" class="btn btn-default ">Back</button>
            </div>
        </form>
    </div>
    </div>
@endsection
