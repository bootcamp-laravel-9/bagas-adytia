<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['session.login']], function () {
    Route::get('/', function () {
        return view('dashboard/index');
    })->name('dashboard');

    Route::get('/login', 'AuthController@login');
    Route::post('/loginUser', 'AuthController@show');
    Route::get('/register', 'AuthController@register');
    Route::post('/registerUser', 'AuthController@create');
    Route::get('/member', 'UserController@index');
    route::get('/detail/{id}', 'UserController@show');
    route::get('/edit/{id}', 'UserController@edit');
    route::post('/update', 'UserController@update');
    route::get('/delete/{id}', 'UserController@destroy');
    Route::get('/resume', 'ResumeController@index');
    Route::get('/logout', 'AuthController@logout');
});

