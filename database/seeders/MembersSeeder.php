<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class MembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members =  [
            ['nama' => 'Bagas', 'univ' => 'Kalimantan', 'asal' => 'Universitas Atma Jaya Yogyakarta'],
            ['nama' => 'Alwi', 'univ' => 'Bangka Belitung', 'asal' => 'Universitas Atma Jaya Yogyakarta'],
            ['nama' => 'Nuel', 'univ' => 'Tegal', 'asal' => 'Universitas Kristen Duta Wacana'],
            ['nama' => 'Kiki', 'univ' => 'Kalimantan', 'asal' => 'Universitas Kristen Duta Wacana'],
            ['nama' => 'Edwin', 'univ' => 'Bangka Belitung', 'asal' => 'Universitas Kristen Duta Wacana'],
            ['nama' => 'Rizal', 'univ' => 'Tegal', 'asal' => 'Universitas Pembangunan Nasional'],
            ['nama' => 'Fatur', 'univ' => 'Kalimantan', 'asal' => 'Universitas Pembangunan Nasional'],
            ['nama' => 'Kristo', 'univ' => 'Bangka Belitung', 'asal' => 'Universitas Atma Jaya Yogyakarta'],
            ['nama' => 'Samuel', 'univ' => 'Tegal', 'asal' => 'Universitas Pembangunan Nasional'],
        ];

        DB::beginTransaction();
        
        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }
        DB::commit();

    }
}