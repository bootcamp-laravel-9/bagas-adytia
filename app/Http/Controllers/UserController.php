<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data=[
        //     ['nama' => 'Bagas', 'univ' => 'Kalimantan', 'asal' => 'Universitas Atma Jaya Yogyakarta'],
        //     ['nama' => 'Alwi', 'univ' => 'Bangka Belitung', 'asal' => 'Universitas Atma Jaya Yogyakarta'],
        //     ['nama' => 'Nuel', 'univ' => 'Tegal', 'asal' => 'Universitas Kristen Duta Wacana'],
        //     ['nama' => 'Kiki', 'univ' => 'Kalimantan', 'asal' => 'Universitas Kristen Duta Wacana'],
        //     ['nama' => 'Edwin', 'univ' => 'Bangka Belitung', 'asal' => 'Universitas Kristen Duta Wacana'],
        //     ['nama' => 'Rizal', 'univ' => 'Tegal', 'asal' => 'Universitas Pembangunan Nasional'],
        //     ['nama' => 'Fatur', 'univ' => 'Kalimantan', 'asal' => 'Universitas Pembangunan Nasional'],
        //     ['nama' => 'Kristo', 'univ' => 'Bangka Belitung', 'asal' => 'Universitas Atma Jaya Yogyakarta'],
        //     ['nama' => 'Samuel', 'univ' => 'Tegal', 'asal' => 'Universitas Pembangunan Nasional'],
        // ];
        // $data = Member::all();

        $data = Member::all();
        return view('member/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Member::find($id);

        // dd($detail);
        return view('member/show', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Member::find($id);

        return view('member/edit', compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $data = Member::find($request->idMember);
        $data->nama = $request->namaMember;
        $data->asal = $request->asalMember;
        $data->univ = $request->univ;
        $data->save();
        return redirect('/member')->with('success', 'Data Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::destroy($id);
        return redirect('/member');
    }
}
