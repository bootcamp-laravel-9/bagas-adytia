@extends('layout/main')
@section('menu-member', 'active')
@section('title', 'Member')
@section('content')
<div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Orang</h3>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Asal Universitas</th>
                      <th>Asal Daerah</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($data as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row['nama'] }}</td>
                            <!-- <td>{{ $row['asal'] }}</td> -->
                            <td>
                                @if($row['asal'] == 'Universitas Atma Jaya Yogyakarta')
                                  <span class="badge badge-success">{{ $row['asal'] }}</span>
                                @elseif ($row['asal'] == 'Universitas Pembangunan Nasional') 
                                  <span class="badge badge-danger">{{ $row['asal'] }}</span>
                                @else
                                  <span class="badge badge-info">{{ $row['asal'] }}</span>
                                @endif
                            </td>
                            <td>{{ $row['univ'] }}</td>
                                <td><a href="{{ url('/detail/' . $row->id)}}" class="btn btn-sm btn-primary">Detail</a></td>
                                <td><a href="{{ url('/edit/' . $row->id)}}" class="btn btn-sm btn-info">Edit</a></td>
                                <td><a href="{{ url('/delete/' . $row->id)}}" class="btn btn-sm btn-danger">Delete</a></td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

@endsection
