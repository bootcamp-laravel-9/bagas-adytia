@extends('layout/main')
@section('menu-member', 'active')
@section('title', 'Member')
@section('content')

    <div class="card card-info">
        <div class="card-header">
        <h3 class="card-title">Detail Form</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form class="form-horizontal">
        <div class="card-body">
            <div class="col-sm-5">
                <h4>Nama</h4>
                <input type="text" class="form-control" value="{{$detail['nama']}}" readonly >
            </div>
            <div class="col-sm-5">
                <h4>Universitas</h4>
                <input type="text" class="form-control" value="{{$detail['asal']}}" readonly >
            </div>
            <div class="col-sm-5">
                <h4>Asal</h4>
                <input type="text" class="form-control" value="{{$detail['univ']}}" readonly >
            </div>
        </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Sign in</button>
            <button type="submit" class="btn btn-default float-right">Cancel</button>
        </div>
        <!-- /.card-footer -->
        </form>
    </div>
    </div>
@endsection
