<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
class AuthController extends Controller
{
    public function login()
    {
        return view('auth/login');
    }

    public function register()
    {
        return view('auth/register');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'max:100'],
            'email' => ['required', 'max:50', 'email', 'unique:users,email'],
            'password' => ['required', 'min:8'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password'])
        ]);

        return redirect('/login')->with('success', 'Register Berhasil');
    }

    public function show(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if(!$user) {
            return redirect('/login')->with('error', 'Email/password Tidak Ditemukan');
        }

        $isValidPassword = Hash::check($request->password, $user->password);

        if(!$isValidPassword) {
            return redirect('/login')->with('error', 'Email/password Tidak Ditemukan');
        }

        $token = $user->createToken(config('app.name'))->plainTextToken;
        $request->session()->put('LoginSession', $token);
        return redirect('/')->with('success', 'Login Berhasil');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
