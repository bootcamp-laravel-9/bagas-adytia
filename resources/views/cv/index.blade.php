@extends('layout/main') 
@section('menu-resume', 'active')
@section('title', 'Resume')
@section('content')

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Resume</title>
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        />
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        />
        <style>
            /* Custom Styles */
            .personal-info {
                margin-top: 30px;
            }

            .personal-info .section-title {
                border-bottom: 2px solid #2e3e78;
                padding-bottom: 10px;
                margin-bottom: 20px;
            }

            .personal-info .section-title h2 {
                color: #2e3e78;
            }

            .personal-info p {
                margin-bottom: 10px;
            }

            .personal-info p strong {
                font-weight: bold;
            }
            body {
                background-color: #f8f9fa;
            }
            .resume {
                background-color: #fff;
                border-radius: 10px;
                box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
                padding: 30px;
                margin-top: 30px;
                margin-bottom: 30px;
            }
            .section-title {
                border-bottom: 2px solid #2e3e78;
                padding-bottom: 10px;
                margin-bottom: 20px;
            }
            .section-title h2 {
                color: #2e3e78;
            }
            .job-title {
                font-weight: bold;
                color: #2e3e78;
            }
            .date {
                color: #6c757d;
            }
            .skills {
                margin-top: 30px;
            }

            .skills .section-title {
                border-bottom: 2px solid #2e3e78;
                padding-bottom: 10px;
                margin-bottom: 20px;
            }

            .skills .section-title h2 {
                color: #2e3e78;
            }

            .skills .list-group-item {
                background-color: #f8f9fa;
                border: none;
                color: #495057;
                font-weight: bold;
            }

            .skills .list-group-item:hover {
                background-color: #e9ecef;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="">
                    <div class="resume">
                        <div class="text-center mb-4">
                            <img
                                src="{{ asset('images/photo.png') }}"
                                alt="Your Photo"
                                style="width: 250px; height: 250px"
                            />
                        </div>
                        <div class="section personal-info">
                            <div class="section-title">
                                <h2>Personal Information</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>Name:</strong> Bagas Adytia</p>
                                    <p>
                                        <strong>Email:</strong>
                                        bagas@email.com
                                    </p>
                                    <p><strong>Phone:</strong> 0812lalala</p>
                                    <p>
                                        <strong>Address:</strong> 
                                        DI Jokesja
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="section">
                            <div class="section-title">
                                <h2>Summary</h2>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Nullam sed feugiat magna.
                                Suspendisse potenti.
                            </p>
                        </div>
                        <div class="section">
                            <div class="section-title">
                                <h2>Education</h2>
                            </div>
                            <div class="item">
                                <h4>University of Atma Jaya Yogyakarta</h4>
                                <p>Bachelor of Science in Computer Science</p>
                                <p class="date">Graduated: Aug 2024</p>
                            </div>
                        </div>
                        <div class="section">
                            <div class="section-title">
                                <h2>Experience</h2>
                            </div>
                            <div class="item">
                                <h4 class="job-title">
                                    Front End Developer / travelxism.com
                                </h4>
                                <p class="date">Jul 2023 - Dec 2023</p>
                                <p>
                                    Lorem ipsum dolor sit amet. Praesentium
                                    magnam consectetur vel in deserunt
                                    aspernatur est reprehenderit sunt hic. Nulla
                                    tempora soluta ea et odio, unde doloremque
                                    repellendus iure, iste.
                                </p>
                            </div>
                            <div class="item">
                                <h4 class="job-title">
                                    Front End Developer / funrobo.id
                                </h4>
                                <p class="date">Dec 2023 - Jan 2024</p>
                                <p>
                                    Lorem ipsum dolor sit amet. Praesentium
                                    magnam consectetur vel in deserunt
                                    aspernatur est reprehenderit sunt hic. Nulla
                                    tempora soluta ea et odio, unde doloremque
                                    repellendus iure, iste.
                                </p>
                            </div>
                            <div class="item">
                                <h4 class="job-title">
                                    Front End Developer/ Berijalan
                                </h4>
                                <p class="date">
                                    Februari 2024 -
                                    <span class="badge badge-primary"
                                        >Current</span
                                    >
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet. Praesentium
                                    magnam consectetur vel in deserunt
                                    aspernatur est reprehenderit sunt hic. Nulla
                                    tempora soluta ea et odio, unde doloremque
                                    repellendus iure, iste.
                                </p>
                            </div>
                        </div>
                        <div class="section">
                            <div class="section-title">
                                <h2>Certificate</h2>
                            </div>
                            <div class="item">
                                <h4>
                                    Belajar Fundamental Front-End Web
                                    Development
                                </h4>
                                <p>Dicoding Bootcamp</p>
                                <p class="date">Expries May 2025</p>
                            </div>
                            <div class="item">
                                <h4>Belajar .NET Dasar</h4>
                                <p>Berijalan Bootcamp</p>
                                <p class="date">Expries May 2025</p>
                            </div>
                            <div class="item">
                                <h4>Belajar React JS</h4>
                                <p>Berijalan Bootcamp</p>
                                <p class="date">Expries May 2025</p>
                            </div>
                        </div>
                        <div class="section ">
                            <div class="section">
                                <div class="section-title">
                                    <h2>Skills</h2>
                                </div>
                                <ul class="list-group skills">
                                    <li class="list-group-item">
                                        HTML/CSS
                                        <div class="progress">
                                            <div
                                                class="progress-bar bg-success"
                                                role="progressbar"
                                                style="width: 50%"
                                                aria-valuenow="90"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                50%
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        JavaScript
                                        <div class="progress">
                                            <div
                                                class="progress-bar bg-warning"
                                                role="progressbar"
                                                style="width: 50%"
                                                aria-valuenow="80"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                50%
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        PHP
                                        <div class="progress">
                                            <div
                                                class="progress-bar bg-info"
                                                role="progressbar"
                                                style="width: 50%"
                                                aria-valuenow="70"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                50%
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        Laravel
                                        <div class="progress">
                                            <div
                                                class="progress-bar bg-primary"
                                                role="progressbar"
                                                style="width: 50%"
                                                aria-valuenow="85"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                50%
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        React
                                        <div class="progress">
                                            <div
                                                class="progress-bar bg-danger"
                                                role="progressbar"
                                                style="width: 50%"
                                                aria-valuenow="75"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                50%
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
@endsection
